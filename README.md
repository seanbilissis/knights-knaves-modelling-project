# CISC/CMPE 204 Modelling Project 53: Knights and Knaves

This project models a puzzle which takes place on an island inhabited by two groups of people: Knights, who always tell the truth, and Knaves, who always tell lies. The object of the game is to determine who the Knaves and Knights are by analyzing the statements they make about the other inhabitants. The code will determine whether each person is truthful (Knight) or a liar/is untruthful (Knave) given a certain set of statements that are defined as constraints to the problem.

## Structure

* `documents`: Contains folders for draft and final submissions.
* `modelling_report.docx`: Contains part D5 of deliverables as outlined in the project description
* `ProjectProofs.jp`: Jape proofs for part D3 of deliverables
* `run.py`: Code used for model and exploration
