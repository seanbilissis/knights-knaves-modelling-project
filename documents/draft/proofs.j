﻿CONJECTUREPANEL Conjectures
CURRENTPROOF "T1, T2, ¬T3 ⊢ T1→(¬T2∨¬T3)"
INFER T1,
     T2,
     ¬T3 
     ⊢ T1→(¬T2∨¬T3)
IS
NEXTGOAL
END
CONJECTUREPANEL Conjectures
CURRENTPROOF "¬T1, ¬T2, T3 ⊢ ¬T1→¬(¬T2∧¬T3)"
INFER ¬T1,
     ¬T2,
     T3 
     ⊢ ¬T1→¬(¬T2∧¬T3)
IS
NEXTGOAL
END
CONJECTUREPANEL Conjectures
CURRENTPROOF "P1, ¬P1, T1→P1, ¬T1→¬P1, T2→P1, ¬T2→¬P1 ⊢ (T1∧¬T2)∨(¬T1∧T2)"
INFER P1,
     ¬P1,
     T1→P1,
     ¬T1→¬P1,
     T2→P1,
     ¬T2→¬P1 
     ⊢ (T1∧¬T2)∨(¬T1∧T2)
IS
NEXTGOAL
END
